const mongoose = require('mongoose');

const designSchema = mongoose.Schema(
	{
		name: {
			type: String,
			text: true,
			required: true
		},
		description: {
			type: String,
			text: true,
			required: true
		},
		puht: {
			type: String,
			required: true
		},
		range: {
			type: String,
			required: true
		},
		ingredients: [[
            {
                id: {
					type: mongoose.Schema.Types.ObjectId,
					ref: 'Ingredient',
					required: true
                },
            }
        ]],
	}
);

module.exports = mongoose.model('Design', designSchema);