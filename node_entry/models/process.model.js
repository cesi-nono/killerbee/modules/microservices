const mongoose = require('mongoose');

const processSchema = mongoose.Schema(
	{
		name: {
			type: String,
			text: true,
			required: true,
		},
		description: {
			type: String,
			text: true,
            required: true,
		},
        protocol: {
            type: String,
            text: true,
            required: true,
        },
        design: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Design',
            required: true
        }
	}
);

module.exports = mongoose.model('Process', processSchema);