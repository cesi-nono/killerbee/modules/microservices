const mongoose = require('mongoose');

const ingredientSchema = mongoose.Schema(
	{
		name: {
			type: String,
			text: true,
			required: true,
		},
		description: {
			type: String,
			text: true,
			required: true,
		}
	}
);

module.exports = mongoose.model('Ingredient', ingredientSchema);
