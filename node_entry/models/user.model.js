const mongoose = require('mongoose');

const userSchema = mongoose.Schema(
	{
		email: {
			type: String,
			text: true,
			required: true,
            unique: true
		},
		password: {
			type: String,
			text: true,
            required: true
		}
	}
);

module.exports = mongoose.model('User', userSchema);