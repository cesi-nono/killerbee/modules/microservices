const controller = require('../controllers/ingredient.controller');

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });

  app.get(
    '/ingredients',
    controller.ingredients
  )

  app.get(
    '/ingredients/:id',
    controller.viewIngredient
  );

  app.post(
    '/ingredients/add',
    controller.addIngredient
  );

  app.delete(
    '/ingredients/:id',
    controller.deleteIngredient
  );

  app.put(
    '/ingredients/:id',
    controller.updateIngredient
  );
}