const controller = require('../controllers/process.controller');

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });

  app.get(
    '/processes',
    controller.processes
  )

  app.get(
    '/processes/:id',
    controller.viewProcess
  );

  app.post(
    '/processes/add',
    controller.addProcess
  );

  app.delete(
    '/processes/:id',
    controller.deleteProcess
  );

  app.put(
    '/processes/:id',
    controller.updateProcess
  );
};