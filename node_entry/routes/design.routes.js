const controller = require('../controllers/design.controller');

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });

  app.get(
    '/designs',
    controller.designs
  )

  app.get(
    '/designs/:id',
    controller.viewDesign
  );

  app.post(
    '/designs/add',
    controller.addDesign
  );

  app.delete(
    '/designs/:id',
    controller.deleteDesign
  );

  app.put(
    '/designs/:id',
    controller.updateDesign
  );
}