const mongoose = require('mongoose');
const mongoDB = 'mongodb://mongodb.killerbee:27017/mongodb';

mongoose.set('debug', true);

module.exports = async() => {
    mongoose.connect(mongoDB, {dbName: 'mongodb'})
    .then(() => {
        console.log('Successfully connected to Mongo Database');
    })
    .catch(err => {
        console.log('Connection error', err);
    })
};