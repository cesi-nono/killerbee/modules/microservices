const User = require('../models/user.model');
var bcrypt = require("bcryptjs");

exports.createUser = async (req, res) => {
    try {
        const alrExists = await User.exists({name: req.body.email});
        if (alrExists) {
          return res.status(304).json({msg: 'User already exists'});
        } else {
          const user = await User.create(
            {
              email: req.body.email,
              password: await bcrypt.hash(req.body.password, 10)
            });
          user.save();
          return res.status(200).json({
            code: 'Server Notification',
            message: 'New User Uploaded Successfully',
            id: user._id,
            dsg: user
          });
        };
      }
      catch (e) {
        console.log(e)
        return res.status(500).json({
          msg: 'Server error',
        });
      }
}

exports.login = (req,res) => {
    User.findOne({
        email: req.body.email
    })
    .exec((err,user) => {
        if (err) {
            res.status(500).send({message: err});
            return;
        }
        if (!user) {
            return res.status(404).send({message: "User not found"});
        }
        
        var passwordIsValid = bcrypt.compare(
            req.body.password,
            user.password
          );
    
        if (!passwordIsValid) {
            return res.status(401).send({
              message: "Invalid Password!"
            });
        }
        else {
            return res.status(200).json({
                success: true,
                email: req.body.email,
            })
        }
    })
}