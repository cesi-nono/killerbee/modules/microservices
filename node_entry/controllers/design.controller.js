const Design = require('../models/design.model');

exports.designs = async (req, res) => {
  try {
    const designs = await Design.find();
    return res.status(200).json(designs)
  } catch (e) {
    return res.status(500).json({
      msg: 'Server error'
    });
  }
};

exports.viewDesign = async (req, res) => {
  try {
    var id = req.params.id;
    const design = await Design.findOne({_id: id});
    return res.status(200).json({
      design,
    });
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.addDesign = async (req, res) =>{ 
  try {
    const alrExists = await Design.exists({name: req.body.name});
    if (alrExists) {
      return res.status(304).json({msg: 'Design already exists'});
    } else {
      const design = await Design.create(
        {
          name: req.body.name,
          description: req.body.description,
          puht: req.body.puht,
          range: req.body.range,
          ingredients: req.body.ingredients
        });
      await design.populate('ingredients');
      design.save();
      return res.status(200).json({
        code: 'Server Notification',
        message: 'New Design Uploaded Successfully',
        id: design._id,
        dsg: design
      });
    };
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.deleteDesign = async (req, res) => {
  try {
    if((req.body).hasOwnProperty('name')) {
      const dupNumb = await Design.countDocuments(req.body)
      if (dupNumb > 1) {
        await Design.deleteMany(req.body);
        return res.json({
          code: 'Server Notification',
          message: 'Design Deletion Successfull',
          name: req.body.name,
        });
      } else if (dupNumb === 1) {
        await Design.deleteOne(req.body);
        return res.json({
          code: 'Server Notification',
          message: 'Design Deletion Successfull',
          name: req.body.name,
        });
      } else {
        return res.status(400).json({
          code: 'Server Notification',
          message: `Does not exists`,
        });
      }
    } else {
      return res.status(400).json({
        code: 'Server Notifiaction',
        message: 'Please specifiy a name'
      })
    } 
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.updateDesign = async (req, res) => {
  try {
    const design = req.body;
    const id = req.params.id;
    var doc = await Design.findByIdAndUpdate(id, design)
    if ( doc.nModified == 0 && doc.n > 0 ){
      return res.status(304).json({
        code: 'Server Notification',
        message: 'This request has already been processed.'
      });
    } else if ( doc.nModified > 0 && doc.n > 0 ){
      return res.status(200).json({
        code: 'Server Notification',
        message: 'Design Update Successfull'
      });
    } else if ( doc.nModified == 0 && doc.n == 0){
      return res.status(304).json({
        code: 'Server Notification',
        message: 'This request is not valid, no match'
      });
    } else {
      return res.status(500).json({
        code: 'Server Notification',
        message: 'Server error'
      });
    }
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
}
