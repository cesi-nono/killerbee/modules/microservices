const Ingredient = require('../models/ingredient.model');

exports.ingredients = async (req, res) => {
  try {
    const ingredients = await Ingredient.find();
    return res.status(200).json(ingredients)
  } catch (e) {
    return res.status(500).json({
      msg: 'Server error'
    });
  }
};

exports.viewIngredient = async (req, res) => {
  try {
    var id = req.params.id;
    const ingredient = await Ingredient.findOne({_id: id});
    return res.status(200).json({
      ingredient,
    });
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.addIngredient = async (req, res) =>{ 
  try {
    const alrExists = await Ingredient.exists({name: req.body.name});
    if (alrExists) {
      return res.status(304).json({msg: 'Ingredient already exists'});
    } else {
      const ingredient = await Ingredient.create(
        {
          name: req.body.name,
          description: req.body.description,
        });
      ingredient.save();
      return res.status(200).json({
        code: 'Server Notification',
        message: 'New Ingredient Uploaded Successfully',
        id: ingredient._id,
        igrd: ingredient
      });
    };
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.deleteIngredient = async (req, res) => {
  try {
    if((req.body).hasOwnProperty('name')) {
      const dupNumb = await Ingredient.countDocuments(req.body)
      if (dupNumb > 1) {
        await Ingredient.deleteMany(req.body);
        return res.status(200).json({
          code: 'Server Notification',
          message: 'Ingredient Deletion Successfull',
          name: req.body.name,
        });
      } else if (dupNumb === 1) {
        await Ingredient.deleteOne(req.body);
        return res.status(200).json({
          code: 'Server Notification',
          message: 'Ingredient Deletion Successfull',
          name: req.body.name,
        });
      } else {
        return res.status(400).json({
          code: 'Server Notification',
          message: `Does not exists`,
        });
      }
    } else {
      return res.status(400).json({
        code: 'Server Notification',
        message: 'Please specify a name'
      })
    }
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.updateIngredient = async (req, res) => {
  try {
    const ingredient = req.body;
    const id = req.params.id;
    var doc = await Ingredient.findByIdAndUpdate(id, ingredient)
    if ( doc.nModified == 0 && doc.n > 0 ){
      return res.status(304).json({
        code: 'Server Notification',
        message: 'This request has already been processed.'
      });
    } else if ( doc.nModified > 0 && doc.n > 0 ){
      return res.status(200).json({
        code: 'Server Notification',
        message: 'Ingredient Update Successfull'
      });
    } else if ( doc.nModified == 0 && doc.n == 0){
      return res.status(304).json({
        code: 'Server Notification',
        message: 'This request is not valid, no match'
      });
    } else {
      return res.status(500).json({
        code: 'Server Notification',
        message: 'Server error'
      });
    }
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
}
