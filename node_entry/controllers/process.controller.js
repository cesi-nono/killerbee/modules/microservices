const Process = require('../models/process.model');

exports.processes = async (req, res) => {
  try {
    const processes = await Process.find();
    return res.status(200).json(processes)
  } catch (e) {
    return res.status(500).json({
      msg: 'Server error'
    });
  }
};

exports.viewProcess = async (req, res) => {
  try {
    var id = req.params.id;
    const process = await Process.findOne({_id: id});
    return res.status(200).json({
      process,
    });
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.addProcess = async (req, res) =>{ 
  try {
    const alrExists = await Process.exists({name: req.body.name});
    if (alrExists) {
      return res.status(304).json({msg: 'Process already exists'});
    } else {
      const process = await Process.create(
        {
          name: req.body.name,
          description: req.body.description,
          protocol: req.body.protocol,
          design: req.body.design
        });
      await process.populate('design');
      process.save();
      return res.status(200).json({
        code: 'Server Notification',
        message: 'New Process Uploaded Successfully',
        id: process._id,
        prc: process
      });
    };
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.deleteProcess = async (req, res) => {
  try {
    if((req.body).hasOwnProperty('name')) {
      const dupNumb = await Process.countDocuments(body)
      if (dupNumb > 1) {
        await Process.deleteMany(body);
        return res.status(200).json({
          code: 'Server Notification',
          message: 'Process Deletion Successfull',
          name: req.body.name,
        });
      } else if (dupNumb === 1) {
        await Process.deleteOne(body);
        return res.status(200).json({
          code: 'Server Notification',
          message: 'Process Deletion Successfull',
          name: req.body.name,
        });
      } else {
        return res.status(400).json({
          code: 'Server Notification',
          message: `Does not exists`,
        });
      }
    } else {
      return res.status(400).json({
        code: 'Server Notification',
        msg: 'Please specify a name'
      });
    }
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
};

exports.updateProcess = async (req, res) => {
  try {
    const process = req.body;
    const id = req.params.id;
    var doc = await Process.findByIdAndUpdate(id, process)
    if ( doc.nModified == 0 && doc.n > 0 ){
      return res.status(304).json({
        code: 'Server Notification',
        message: 'This request has already been processed.'
      });
    } else if ( doc.nModified > 0 && doc.n > 0 ){
      return res.status(200).json({
        code: 'Server Notification',
        message: 'Process Update Successfull'
      });
    } else if ( doc.nModified == 0 && doc.n == 0){
      return res.status(304).json({
        code: 'Server Notification',
        message: 'This request is not valid, no match'
      });
    } else {
      return res.status(500).json({
        code: 'Server Notification',
        message: 'Server error'
      });
    }
  }
  catch (e) {
    console.log(e)
    return res.status(500).json({
      msg: 'Server error',
    });
  }
}
