const express = require('express'),
  app = express(),
  cors = require('cors'),
  corsOptions = { origin: 'http://localhost:8080' },
  bodyParser = require('body-parser'),
  connectDB = require('./config/db');


connectDB();
app.use(cors(/*corsOptions*/));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
require('./config/db')(app);
require('./routes/auth.routes')(app);
require('./routes/design.routes')(app);
require('./routes/ingredient.routes')(app);
require('./routes/process.routes')(app);

module.exports = app;